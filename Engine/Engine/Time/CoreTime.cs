﻿// Copyright (C) 2017 Roderick Griffioen
// This file is part of the "Core Engine".
// For conditions of distribution and use, see copyright notice in Core.cs

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreEngine.Engine.Time
{
    /// <summary>
    /// Main Time class
    /// </summary>
    public class CoreTime
    {
        #region Data
        public static double deltaTime;
        #endregion
    }
}
