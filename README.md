![Core Logo](Images/CoreLogoSide_HD_Dark_Resized.png)

Website: TBA

Email: roderick.griffioen@gmail.com

# Changelog
- Wiki now contains sample code!
- Components working properly now, they are saved as they should, including the resources they might use
- Check the latest commit message to see the latest changes, screenshots and blog about the progress are in the makings!

#### Isn't this engine also being made in C++?
Yes, yes it is. I opted for trying this out in C# too since I am getting more fluent in C# and am finding it easier to work in so far.  
I might find out that C++ is still the way to go, but I want to try this direction for a bit.

#### Why Core Engine?

The Core Engine is a powerful cross platform. Written in C# it delivers great performance and great quality.
The engine is currently in very early baby stage. The community can pitch in and support me in building this engine (I am a one man dev team).
I stream making parts of this engine on twitch where you can join in and have some chats or see how I go about making this engine.

#### Core Tech
When the engine is finished it will be/contain:

- Consistent 2D/3D API available in C#
- Completely open source and community fed.
- High performance engine core written in C#.
- 2D and 3D physics available through Box2D and PhysX.
- High quality sound engine.
- Highly functional editor to create maps and edit resources.
- Tools to create AI, Animation setups, scripts etc.
- Visual code editor.
- Visual material editor.
- Core Engine Shading Language (CESL) for writing cross platform shaders.
- And much more, expect this list to grow over time!

---

#### Core Engine Roadmap
Do you want to know what is planned in the future for the Core Engine?
[Trello Roadmap](https://trello.com/b/OCBe57G3/core-engine-roadmap)

---

#### Looking For:
I'm looking for people that like to help out with this engine, I don't require you to be full time, if you can spare a few hours a week to check/write code, implement data structures, design architectures or help me think about design and future additions I would be very pleased.

Right now I need the most:
- Proficient Direct X programmer (Currently I have abandoned DX dev, it's taking me too much time to get DX on the same level as GL, if I come across a willing DX dev, it's back on the table)
- Artist to help create art for the editor and possibly website.
- Code Architecture designer to help properly structure the engine.
- You! I need anyone who wants to help out and make this a great engine, if you feel like you can offer something great to this engine, write me a line!

---

#### Documentation

Currently only local after cloning the repo, will be online soon! 

---

Logo is made by Konstantinos Mourelas
